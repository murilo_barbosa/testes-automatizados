from features.utils import logger, constants, global_variables as gv, file_comum as fc
from features.support.enviroments.SetEnvironments import SetEnvironments


def before_all(context):
    env = context.config.userdata['env']
    SetEnvironments(env)
    logger.logging.info(logger.LAYOUT_LOGS)
    logger.logging.info(constants.AMBIENTE_DE_EXECUCAO % env.upper())
    # gv.headless = context.config.userdata['headless']
    # gv.browser = context.config.userdata['nav']
    # if headless == '':
    #     gv.headless = headless
    # if browser != '':
    #     gv.browser = browser


def before_feature(context, feature):
    logger.logging.info(logger.LAYOUT_LOGS)
    logger.logging.info(constants.INICIANDO_SUITE_DE_TESTE)
    logger.logging.info(logger.LAYOUT_LOGS)
    logger.logging.info(feature)


def before_scenario(context, scenario):
    logger.logging.info(logger.LAYOUT_LOGS)
    logger.logging.info(constants.INICIANDO_CASO_DE_TESTE)
    logger.logging.info(logger.LAYOUT_LOGS)
    logger.logging.info(scenario)
    fc.remover_imagens_temporarias()


def after_scenario(context, scenario):
    gv.driver = None
    fc.gerar_pdf(scenario)
    fc.remover_imagens_temporarias()
    logger.logging.info(logger.LAYOUT_LOGS)
    logger.logging.info(constants.FINALIZANDO_CASO_DE_TESTE)
    logger.logging.info(logger.LAYOUT_LOGS)


def after_feature(context, feature):
    if gv.driver is not None:
        gv.driver.quit()
    gv.driver = None
    logger.logging.info(logger.LAYOUT_LOGS)
    logger.logging.info(constants.FINALIZANDO_SUITE_DE_TESTE)
    logger.logging.info(logger.LAYOUT_LOGS)


def after_all(context):
    if gv.driver is not None:
        gv.driver.quit()
    gv.driver = None
    logger.logging.info(logger.LAYOUT_LOGS)
    logger.logging.info(constants.FINALIZANDO_EXECUCAO_DOS_TESTES)
    logger.logging.info(logger.LAYOUT_LOGS)
