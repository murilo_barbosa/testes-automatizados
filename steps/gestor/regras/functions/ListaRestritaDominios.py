import time
from bs4 import BeautifulSoup
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from features.utils import file_comum as fc, logger, constants


class ListaRestritaDominios(object):
    def __init__(self):
        self.driver = fc.get_driver()
        self.texto_botao_configurar = "Configurar"
        self.texto_botao_habilitar = "Habilitar"
        self.texto_botao_desabilitar = "Desabilitar"
        self.texto_janela_inicial = "Lista Restrita Domínios"
        self.texto_botao_habilitar_regra = 'Habilitar Regra'
        self.texto_botao_desabilitar_regra = 'Desabilitar Regra'
        self.wait = WebDriverWait(self.driver, 5, poll_frequency=1)
        self.multiplos_dominios = 'test1.com;test2.com;test3.com'

    def clicar_botao_configurar(self):
        clicar_botao = constants.CLICANDO_BOTAO % self.texto_botao_configurar
        try:
            self.driver.save_screenshot(fc.name_screenshot_default(clicar_botao))
            self.driver.find_element(By.XPATH,
                                     '/html/body/section/div/div[2]/div[2]/div[1]/div[2]/div/div[2]/div[20]/div/button[1]').click()
            logger.logging.info(clicar_botao)
        except Exception as e:
            logger.logging.error(constants.ERRO_CLICAR_BOTAO % self.texto_botao_configurar)
            logger.logging.error(e)
            raise

    def clicar_botao_desabilitar(self):
        time.sleep(1)
        elemento = '/html/body/section/div/div[2]/div[2]/div[1]/div[2]/div/div[2]/div[20]/a'
        clicar_botao = constants.CLICANDO_BOTAO % self.texto_botao_desabilitar
        try:
            self.driver.save_screenshot(fc.name_screenshot_default(clicar_botao))
            self.driver.find_element(By.XPATH, elemento).click()
            logger.logging.info(clicar_botao)
        except Exception as e:
            logger.logging.error(constants.ERRO_CLICAR_BOTAO % self.texto_botao_desabilitar)
            logger.logging.error(e)
            raise

    def clicar_botao_confirmar_desabilitar_regra(self):
        time.sleep(1)
        elemento = 'button[class="btn btn-danger"]'
        clicar_botao = constants.CLICANDO_BOTAO % self.texto_botao_desabilitar_regra
        try:
            self.driver.save_screenshot(fc.name_screenshot_default(clicar_botao))
            self.driver.find_element(By.CSS_SELECTOR, elemento).click()
            logger.logging.info(clicar_botao)
        except Exception as e:
            logger.logging.error(constants.ERRO_CLICAR_BOTAO % self.texto_botao_desabilitar_regra)
            logger.logging.error(e)
            raise

    def clicar_botao_habilitar(self):
        elemento = '/html/body/section/div/div[2]/div[2]/div[1]/div[2]/div/div[2]/div[19]/div/button[2]'
        time.sleep(1)
        clicar_botao = constants.CLICANDO_BOTAO % self.texto_botao_habilitar
        try:
            self.driver.save_screenshot(fc.name_screenshot_default(clicar_botao))
            self.driver.find_element(By.XPATH, elemento).click()
            logger.logging.info(clicar_botao)
        except Exception as e:
            logger.logging.error(constants.ERRO_CLICAR_BOTAO % self.texto_botao_habilitar)
            logger.logging.error(e)
            raise

    def clicar_botao_confirmar_habilitar_regra(self):
        elemento = '#modalConfirmEnable button.btn.btn-success'
        time.sleep(1)
        clicar_botao = constants.CLICANDO_BOTAO % self.texto_botao_habilitar_regra
        try:
            self.driver.save_screenshot(fc.name_screenshot_default(clicar_botao))
            self.driver.find_element(By.CSS_SELECTOR, elemento).click()
            logger.logging.info(clicar_botao)
        except Exception as e:
            logger.logging.error(constants.ERRO_CLICAR_BOTAO % self.texto_botao_habilitar_regra)
            logger.logging.error(e)
            raise

    def clicar_botao_reordenar(self):
        botao = 'Reordenar'
        elemento = 'button[class="btn btn-default top-10"'
        clicar_botao = constants.CLICANDO_BOTAO % botao
        try:
            self.driver.save_screenshot(fc.name_screenshot_default(clicar_botao))
            self.driver.find_element(By.CSS_SELECTOR, elemento).click()
            logger.logging.info(clicar_botao)
        except Exception as e:
            logger.logging.error(constants.ERRO_CLICAR_BOTAO % botao)
            logger.logging.error(e)
            raise

    def clicar_botao_nova_restricao(self):
        """Clicando no botão 'Nova Restrição' para incluir uma nova regra de restrição."""
        botao = 'Nova Restrição'
        try:
            self.wait.until((EC.visibility_of(self.driver.find_element(By.ID, 'listContainer'))))
            self.driver.save_screenshot(fc.name_screenshot_default(constants.CLICANDO_BOTAO % botao))
            self.driver.find_element(By.CSS_SELECTOR, 'button[class="btn btn-success"]').click()
            logger.logging.info(constants.CLICANDO_BOTAO % botao)
        except Exception as e:
            logger.logging.error(constants.ERRO_CLICAR_BOTAO % botao)
            logger.logging.error(e)
            raise

    def clicar_botao_salvar(self):
        botao = 'Salvar'
        try:
            self.driver.save_screenshot(fc.name_screenshot_default(constants.CLICANDO_BOTAO % botao))
            self.driver.find_element(By.CLASS_NAME, 'btn-success').click()
            logger.logging.info(constants.CLICANDO_BOTAO % botao)
        except Exception as e:
            logger.logging.error(constants.ERRO_CLICAR_BOTAO % botao)
            logger.logging.error(e)
            raise

    def clicar_botao_excluir(self):
        botao = 'Excluir'
        try:
            self.driver.save_screenshot(fc.name_screenshot_default(constants.CLICANDO_BOTAO % botao))
            self.driver.find_element(By.CLASS_NAME, 'red').click()
            logger.logging.info(constants.CLICANDO_BOTAO % botao)
        except Exception as e:
            logger.logging.error(constants.ERRO_CLICAR_BOTAO % botao)
            logger.logging.error(e)
            raise

    def clicar_botao_confirmar_remover_extensao(self):
        botao = 'Remover'
        try:
            elemento = self.wait.until(
                EC.element_to_be_clickable(self.driver.find_element(By.CLASS_NAME, 'btn.btn-danger')))
            self.driver.save_screenshot(fc.name_screenshot_default(constants.CLICANDO_BOTAO % botao))
            elemento.click()
            logger.logging.info(constants.CLICANDO_BOTAO % botao)
        except Exception as e:
            logger.logging.error(constants.ERRO_CLICAR_BOTAO % botao)
            logger.logging.error(e)
            raise

    def pegar_tabela_regras_lista_restrita_dominios(self):
        """Pega todos os dados da tabela HTML com as regras de DDD ja criadas"""
        try:
            self.wait.until(EC.visibility_of(self.driver.find_element(By.ID, 'listContainer')))
            self.driver.find_element(By.ID, 'listItem')
            try:
                html = self.driver.page_source
                soup = BeautifulSoup(html, 'html.parser')
                titulos_html = soup.find('div', {'class': 'shotTableGrid header-table'}).find_all('p')
                valores_html = soup.find('div', {'id': 'listContainer'}).find_all('p', {'class': 'ng-binding'})
                lista_titulos = [titulos.getText().replace('<p>', "") for titulos in titulos_html[:2]]
                lista_valores = [valores.getText().replace('<p>', "") for valores in valores_html]
                lista = []
                cont = 0
                while cont < len(lista_valores):
                    for valor in [lista_valores[cont:]]:
                        dicionario = dict(zip(lista_titulos, valor))
                        lista.append(dicionario)
                    cont += 3
                return lista
            except Exception as e:
                logger.logging.error(constants.VALOR_NAO_ENCONTRADO % e)
                raise
        except:
            logger.logging.info(constants.LISTA_VAZIA)
            return 'VAZIO'

    def pegar_texto_janela_inicial(self):
        time.sleep(1.5)
        try:
            texto = self.driver.find_element(By.XPATH,
                                             '/html/body/section/div/div[2]/div[2]/div/div[1]/div[1]/h1').text
            self.driver.save_screenshot(fc.name_screenshot_default(texto))
            logger.logging.info(constants.TEXTO_ENCONTRADO_SUCESSO % texto)
            return texto
        except Exception as e:
            logger.logging.error(constants.ERRO_ENCONTRAR_TEXTO % self.texto_janela_inicial)
            logger.logging.error(e)
            raise

    def preencher_campo_dominio(self, dominio):
        texto_campo = 'Extensão Domínio'
        try:
            elemento = self.wait.until(EC.visibility_of(self.driver.find_element(By.ID, 'itensToAdd')))
            self.driver.save_screenshot(
                fc.name_screenshot_default(constants.DIGITANDO_TEXTO_CAMPO % (dominio, texto_campo)))
            elemento.send_keys(str(dominio))
            logger.logging.info(constants.DIGITANDO_TEXTO_CAMPO % (dominio, texto_campo))
        except Exception as e:
            logger.logging.error(e)
            raise

    def validar_status_regra(self, status):
        time.sleep(1)
        elemento = '/html/body/section/div/div[2]/div[2]/div[1]/div[2]/div/div[2]/div[19]/div/button[2]'
        try:
            self.driver.save_screenshot(fc.name_screenshot_default(f'Regra {status}'))
            self.driver.find_element(By.XPATH, elemento)
            return status
        except Exception as e:
            logger.logging.error(e)
            raise

    def validar_pagina_nova_restricao(self):
        texto = None
        try:
            texto = self.wait.until(EC.visibility_of(self.driver.find_element(By.CSS_SELECTOR, 'h1[style]'))).text
            self.driver.save_screenshot(
                fc.name_screenshot_default(constants.PAGINA_VALIDADA_SUCESSO % texto))
            return texto
        except Exception as e:
            logger.logging.error(constants.ERRO_VALIDAR_PAGINA % texto)
            logger.logging.error(e)
            raise
