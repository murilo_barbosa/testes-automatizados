import time
from selenium.webdriver.common.by import By
from features.utils import file_comum as fc, logger, constants


class Regras(object):

    def __init__(self):
        self.driver = fc.get_driver()

    def alterar_ordenacao(self, de, para):
        """Alterar o tipo de ordenação, de DDD para Estado e Estado para DDD"""
        update_order = constants.ORDENACAO_ALTERADA_DE_PARA % (de, para)
        try:
            self.driver.find_element(By.LINK_TEXT, para).click()
            self.driver.save_screenshot(fc.name_screenshot_default(update_order))
            logger.logging.info(update_order)
        except Exception as e:
            logger.logging.error(constants.VALOR_NAO_ENCONTRADO % e)
            raise

    def clicar_menu_regras(self):
        """Acessa o menu Regras do menu do Gestor"""
        texto_menu = 'Regras'
        try:
            self.driver.find_element_by_link_text(texto_menu).click()
            time.sleep(1)
            logger.logging.info(constants.CLICANDO_MENU % texto_menu)
        except Exception:
            logger.logging.error(constants.ERRO_CLICAR_MENU % texto_menu)
            raise

    def clicar_dropdown_ordenar_por(self):
        """Clicar no dropdown para selecionar o tipo de ordenação das regras"""
        dropdown = 'Ordenar por'
        elemento = 'btn.btn-default.dropdown-toggle.as-is.bs-dropdown-to-select'
        click_dropdown = constants.CLICANDO_DROPDOWN % dropdown
        try:
            time.sleep(2)
            self.driver.find_element(By.CLASS_NAME, elemento).click()
            self.driver.save_screenshot(fc.name_screenshot_default(click_dropdown))
            logger.logging.info(click_dropdown)
            return click_dropdown
        except Exception as e:
            logger.logging.error(constants.ERRO_CLICAR_DROPDOWN % dropdown)
            logger.logging.error(e)
            raise

    def validar_janela_inicial_regras(self):
        """"Valida se a janela de Regras inicial foi aberta corretamente."""
        try:
            return self.driver.find_element(By.CLASS_NAME, 'col-xs-12.templateTitle').text
        except Exception as e:
            logger.logging.info(e)
            raise

    def verificar_existencia_regra_valida(self):
        elemento = self.driver.find_element(By.XPATH,
                                            '/html/body/section/div/div[5]/div[2]/div[1]/div[2]/div/div[1]/div[20]/div'
                                            '/button[2]')
        if elemento:
            logger.logging.info('Já existe uma regra criada.')
            return elemento
        else:
            logger.logging.error('Elemento não localizado.')

    @staticmethod
    def clicar_ver_detalhes(elemento):
        elemento.click()
