from behave import when, then
from features.steps.gestor.regras.functions.LimitacaoDisparos import LimitacaoDisparos
from features.utils import logger, constants, file_comum as fc

limitacao_disparos = LimitacaoDisparos()
order_name_from = None


@when(u'clicar no botão "{button}" da opção "Limitação Disparos"')
def clicar_botao(context, button):
    global limitacao_disparos
    limitacao_disparos = LimitacaoDisparos()

    match button:
        case 'Configurar':
            limitacao_disparos.click_button_configurar()
        case 'Desabilitar':
            limitacao_disparos.click_button_desabilitar()
            limitacao_disparos.click_button_desabilitar_regra()
        case 'Habilitar':
            limitacao_disparos.click_button_habilitar()
            limitacao_disparos.click_button_habilitar_regra()


@when(u'alterar a ordenação de "{order_name_1}" para "{order_name_2}" na janela de "Limitação Disparos"')
def change_order(context, order_name_1, order_name_2):
    global order_name_from
    limitacao_disparos.click_dropdown()
    if order_name_2 == 'Nome':
        limitacao_disparos.select_option_dropdown(order_name_1)
        limitacao_disparos.click_dropdown()
    limitacao_disparos.select_option_dropdown(order_name_2)
    order_name_from = order_name_1


@when(u'clicar no botão para reordenar a ordem de exibição na janela de "Limitação Disparos"')
def click_button_reorder(context):
    limitacao_disparos.click_button_reorder()


@when(u'realizar uma busca por "{option}" na janela de "Limitação Disparos"')
def filter_option_by_search(context, option):
    limitacao_disparos.filter_option(option)


@then(u'esteja na janela de "Limitação Disparos"')
def access_start_window(context):
    clicar_botao(context, 'Configurar')


@then(u'a janela inicial "Limitação Disparos" das regras deverá ser aberta')
def validar_janela_limitacao_disparo(context):
    try:
        texto_retornado = limitacao_disparos.get_text_inicial_page()
        assert limitacao_disparos.text_initial_page == texto_retornado
        logger.logging.info(constants.PAGINA_VALIDADA_SUCESSO % texto_retornado)
        limitacao_disparos.driver.save_screenshot(
            fc.name_screenshot_default(constants.PAGINA_VALIDADA_SUCESSO % texto_retornado))
    except Exception as e:
        logger.logging.error(constants.ERRO_VALIDAR_PAGINA % limitacao_disparos.text_initial_page)
        logger.logging.error(e)
        raise
    finally:
        limitacao_disparos.driver.quit()


@then(u'a regra da opção "Limitação Disparos" deverá estar "{condition}"')
def validate_rule(context, condition):
    try:
        regra = limitacao_disparos.validate_rule(condition)
        logger.logging.info(regra)
        assert regra is None
        logger.logging.info(constants.PAGINA_VALIDADA_SUCESSO % condition)
        limitacao_disparos.driver.save_screenshot(
            fc.name_screenshot_default(constants.PAGINA_VALIDADA_SUCESSO % condition))
    except Exception as e:
        logger.logging.error(constants.ERRO_VALIDAR_PAGINA % condition)
        logger.logging.error(e)
    finally:
        limitacao_disparos.driver.quit()


@then(u'a ordenação por "{order_name}" deverá ser exibida de forma "crescente" na janela de "Limitação Disparos"')
def check_order_page(context, order_name):
    global order_name_from
    try:
        # TODO Identificar um método eficiente de validar a ordenação.
        limitacao_disparos.validate_order_list()
        limitacao_disparos.driver.save_screenshot(
            fc.name_screenshot_default(constants.ORDENACAO_VALIDADA_SUCESSO % order_name))
        logger.logging.info(constants.ORDENACAO_VALIDADA_SUCESSO % order_name)
    except Exception:
        logger.logging.error(constants.ERRO_VALIDAR_ORDENACAO % (order_name_from, order_name))
        raise
    finally:
        limitacao_disparos.driver.quit()


@then(u'a ordenação deverá ser exibida de forma "decrescente" na janela de "Limitação Disparos"')
def validate_page_order(context):
    page_text = 'Limitação Disparos'
    try:
        limitacao_disparos.validate_order_list()
        limitacao_disparos.driver.save_screenshot(
            fc.name_screenshot_default(constants.PAGINA_VALIDADA_SUCESSO % page_text))
        logger.logging.info(constants.PAGINA_VALIDADA_SUCESSO % page_text)
    except Exception as e:
        logger.logging.error(constants.ERRO_VALIDAR_PAGINA % page_text)
        logger.logging.error(e)
        raise
    finally:
        limitacao_disparos.driver.quit()


@then(u'todas as regras com o/a "{option}" selecionado deverá ser exibida na janela de "Limitação Disparos"')
def validate_filter_by_search(context, option):
    try:
        # TODO Implementar um método mais eficiente de validar se a Busca foi realizada e exibida com sucesso
        #   Talvez por OCR ou Interceptação do JS
        logger.logging.info(constants.PAGINA_VALIDADA_SUCESSO % option)
        limitacao_disparos.driver.save_screenshot(
            fc.name_screenshot_default(constants.PAGINA_VALIDADA_SUCESSO % option))
    except Exception as e:
        logger.logging.error(e)
        raise
    finally:
        limitacao_disparos.driver.quit()
