from behave import when, then
import time
from features.steps.gestor.regras.functions.Ddd import Ddd
from features.utils import logger, constants, file_comum as fc

ddd = Ddd()


@when(u'clicar no botão "{button}" da opção "DDD"')
def clicar_botao_opcao_ddd(context, button):
    global ddd
    ddd = Ddd()
    try:
        match button:
            case ddd.texto_botao_configurar:
                ddd.clicar_botao_configurar_ddd()
            case ddd.texto_botao_desabilitar:
                ddd.clicar_botao_desabilitar_regra()
                text = ddd.validar_janela_confirmacao_desabilitar_habilitar_regra(button)
                if text == button.upper():
                    ddd.clicar_botao_confirmar_desabilitar_regra()
                else:
                    raise
            case ddd.texto_botao_habilitar:
                ddd.clicar_botao_habilitar_regra()
                text = ddd.validar_janela_confirmacao_desabilitar_habilitar_regra(button)
                if text == button.upper():
                    ddd.clicar_botao_confirmar_habilitar_regra()
                else:
                    raise
            case _:
                logger.logging.error(constants.VALOR_NAO_ENCONTRADO % button)
                raise
    except Exception as e:
        logger.logging.error(constants.ERRO_CLICAR_BOTAO % button)
        logger.logging.error(e)
        raise


@when(u'alterar a ordenação de "{condition_1}" para "{condition_2}" na janela de "DDD"')
def alterar_ordenacao(context, condition_1, condition_2):
    ddd.clicar_dropdown_ordenar_por()
    ddd.alterar_ordenacao(condition_1, condition_2)


@when(u'clicar no botão para alterar a ordem de "crescente" para "decrescente" na página de DDD')
def step_impl(context):
    ddd.clicar_botao_reordenar()


@when(u'realizar uma busca por um "{condition}"')
def realizar_busca(context, condition):
    time.sleep(1)
    lista_regras = ddd.pegar_tabela_regras_ddd()
    context.valor_busca = lista_regras[-1][condition]
    ddd.realizar_busca_ddd(context.valor_busca)


@then(u'a janela inicial "DDD" das regras deverá ser aberta')
def validar_janela_inicial_ddd(context):
    texto_esperado = ddd.texto_ddd
    try:
        texto_retornado = Ddd().pegar_texto_janela_ddd()
        assert texto_esperado == texto_retornado
        logger.logging.info(constants.PAGINA_VALIDADA_SUCESSO % texto_retornado)
    except Exception:
        logger.logging.error(constants.ERRO_VALIDAR_PAGINA % texto_esperado)
        raise
    finally:
        ddd.driver.quit()


@then(u'a ordenação por "{condition}" deverá ser exibida de forma "{type_order}" na janela de "DDD"')
def validar_ordenacao_crescente(context, condition, type_order):
    lista = ddd.pegar_tabela_regras_ddd()
    valor_primeira_linha = lista[0][condition]
    valor_segunda_linha = lista[1][condition]
    try:
        qdt_lista = 1
        while valor_primeira_linha == 'Todos':
            valor_primeira_linha = lista[qdt_lista][condition]
            valor_segunda_linha = lista[qdt_lista + 1][condition]
            qdt_lista += 1
            continue
        else:
            if type_order == 'crescente':
                assert valor_primeira_linha <= valor_segunda_linha
            elif type_order == 'decrescente':
                assert valor_primeira_linha >= valor_segunda_linha
            else:
                logger.logging.error(constants.VALOR_NAO_ENCONTRADO % type_order)
                raise
            logger.logging.info(constants.ORDENACAO_VALIDADA_SUCESSO % condition)
            ddd.driver.save_screenshot(
                fc.name_screenshot_default(constants.ORDENACAO_VALIDADA_SUCESSO % condition))
    except Exception:
        logger.logging.error(constants.ERRO_VALIDAR_ORDENACAO % (valor_primeira_linha, valor_segunda_linha))
        raise
    finally:
        ddd.driver.quit()


@then(u'a regra deverá estar "{status}"')
def validar_status_regra(context, status):
    status_retornado = ddd.validar_status_regra(status)
    try:
        assert status_retornado == status
        logger.logging.info(constants.VALIDAR_REGRA % status_retornado)
    except Exception:
        logger.logging.info(constants.ERRO_VALIDAR_REGRA % status_retornado)
        raise
    finally:
        ddd.driver.quit()


@then(u'todas as regras com o "{condition}" selecionado deverá ser exibida')
def validar_busca_regra(context, condition):
    # TODO
    #  Verificar um meio eficiente de validar o temporario_pdf. Pois o HTML é a mesma tabela SEMPRE,
    #  e a busca é filtrada somente no front. Talvez seja necessário fazer um document.query direto pelo JS.
    #  A única diferença notada é que quando exibido o filtro, deixa de existir o atributo "style",
    #  porém a busca sem esse elemento somente pelo Selenium é não é possivel.
    #  O Selenium busca por elementos, e não pela falta deles.
    try:
        pass
    except Exception as e:
        logger.logging.error(e)
        raise
    finally:
        ddd.driver.quit()
