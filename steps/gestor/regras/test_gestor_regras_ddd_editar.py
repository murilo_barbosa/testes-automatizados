from behave import when, then
import time
from features.steps.gestor.regras.functions.Ddd import Ddd
from features.utils import logger, constants, file_comum as fc

ddd = Ddd()
dir_screenshot = 'gestor/regras/ddd/editar_ddd'
status_checkbox_marcado = 'Marcado'
status_checkbox_desmarcado = 'Desmarcado'


@when(u'clicar no botão "Configurar" para editar a regra de um "DDD"')
def clicar_botao_configurar_ddd(context):
    global ddd
    ddd = Ddd()
    ddd.clicar_botao_configurar_ddd()


@when(u'clicar no botão para editar uma regra da opção "DDD"')
def clicar_botao_editar_regra(context):
    ddd.clicar_botao_editar_regra()


@when(u'editar o campo "{field}" no modal "Editar regra por DDD" com o dia "{day}" e salvar')
def editar_campo_modal_editar_regra_por_ddd(context, field, day):
    ddd.modal_editar_regra_ddd()
    context.hora_inicio, context.hora_fim = ddd.acrescentar_hora_periodo(day)
    ddd.alterar_periodo(day, context.hora_inicio, context.hora_fim)
    ddd.clicar_botao_salvar()
    time.sleep(2)


@when(u'desmarcar o checkbox do dia "{day}" do modal de "Editar regra por DDD" e salvar')
def desmarcar_checkbox_editar_regra(context, day):
    ddd.selecionar_checkbox_modal_editar(day)
    ddd.clicar_botao_salvar()


@then(u'o checkbox do dia "{day}" esteja marcado')
def verificar_status_checkbox(context, day):
    status = ddd.verificar_status_checkbox_modal_editar_regra(day)
    time.sleep(1)
    try:
        if status:
            logger.logging.info(constants.STATUS_CHECKBOX % (day, status_checkbox_marcado))
        else:
            logger.logging.info(constants.STATUS_CHECKBOX % (day, status_checkbox_desmarcado))
            editar_campo_modal_editar_regra_por_ddd(context, 'Adicionais', day)
            clicar_botao_editar_regra(context)
            ddd.modal_editar_regra_ddd()
            status = ddd.verificar_status_checkbox_modal_editar_regra(day)
            if status:
                logger.logging.info(constants.STATUS_CHECKBOX % (day, status_checkbox_marcado))
            else:
                raise
    except Exception as e:
        logger.logging.error(constants.ERRO_GENERICO % e)
        raise


@then(u'o modal editar regra "DDD" estiver aberta')
def abrir_modal_editar_regra(context):
    clicar_botao_configurar_ddd(context)
    clicar_botao_editar_regra(context)


@then(u'a janela para editar uma regra da opção "DDD" deverá ser aberta')
def validar_modal_editar_ddd(context):
    texto_esperado = 'EDITAR REGRA POR DDD'
    texto_encontrado = None
    try:
        texto_encontrado = ddd.pegar_texto_modal_editar_ddd()
        assert texto_esperado == texto_encontrado
        logger.logging.info(constants.PAGINA_VALIDADA_SUCESSO % texto_encontrado)
    except Exception:
        logger.logging.error(constants.ERRO_VALIDAR_PAGINA % texto_encontrado)
        raise
    finally:
        ddd.driver.quit()


@then(u'a regra de "DDD" deverá ser editada corretamente com o dia "{day}"')
def validar_alteracao_periodo(context, day):
    try:
        hora_inicio, hora_fim = ddd.pegar_horario_primeira_linha(day)
        assert hora_inicio == context.hora_inicio
        logger.logging.info('Hora inicial igual a hora alterada')
        assert hora_fim == context.hora_fim
        logger.logging.info('Hora final igual a hora alterada')
        ddd.driver.save_screenshot(fc.name_screenshot_default(constants.PAGINA_VALIDADA_SUCESSO % day))
    except Exception as e:
        logger.logging.error('Hora atual divergente com a hora da alteração.')
        logger.logging.error(e)
        raise
    finally:
        ddd.driver.quit()


@then(u'o checkbox de "{day}" do modal de "Editar regra por DDD" deverá ser desmarcado com sucesso')
def validar_checkbox_desmarcado(context, day):
    try:
        status_checkbox = ddd.verificar_status_checkbox_modal_editar_regra(day)
        assert status_checkbox is False
        logger.logging.info(constants.STATUS_CHECKBOX % (day, status_checkbox_desmarcado))
    except Exception as e:
        logger.logging.error(e)
        raise
    finally:
        ddd.driver.quit()
