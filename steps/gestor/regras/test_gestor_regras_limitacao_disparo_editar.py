import time

from behave import when, then
from features.steps.gestor.regras.functions.LimitacaoDisparos import LimitacaoDisparos
from features.utils import logger, constants, file_comum as fc

limitacao_disparos = LimitacaoDisparos()


@when(u'clicar no botão "Editar" da opção "Limitação Disparos"')
def click_edit_button(context):
    global limitacao_disparos
    limitacao_disparos = LimitacaoDisparos()
    limitacao_disparos.click_button_configurar()
    limitacao_disparos.click_button_edit()


@when(u'clicar no botão "Salvar" para adicionar uma nova regra')
def clicar_botao_salvar(context):
    limitacao_disparos.click_button_save_new_rule()
    limitacao_disparos.click_button_confirmar()


@when(u'clicar no botão "Confirmar" para adicionar uma nova regra')
def clicar_btn_confirmar(context):
    limitacao_disparos.confirm_option_selected()


@when(u'alterar o "Nome" da janela de "Editar Limitação de Disparos" e Salvar')
def change_name(context):
    limitacao_disparos.edit_name_text()
    limitacao_disparos.click_button_save_new_rule()
    limitacao_disparos.click_button_confirmar()


@when(u'deixar o campo "Nome" vazio e salvar')
def set_name_empty(context):
    limitacao_disparos.edit_name_text("")
    limitacao_disparos.click_button_save_new_rule()
    limitacao_disparos.click_button_confirmar()


@when(u'selecionar a opção "{option}" do dropdown "Carteira" na janela "Editar Limitação de Disparos" e Salvar')
def select_carteira(context, option):
    limitacao_disparos.select_option_carteira(option)
    limitacao_disparos.click_button_save_new_rule()
    limitacao_disparos.click_button_confirmar()


@when(u'alterar o "Nome" da janela de "Editar Limitação de Disparos" para "Testes" e Cancelar')
def change_name_and_press_cancel_btn(context):
    limitacao_disparos.edit_name_text()
    limitacao_disparos.click_button_cancelar()


@then(u'clicar no botão "Adicionar Nova Regra" na janela "Editar Limitação de Disparos"')
def click_button_adicionar_nova_regra(context):
    limitacao_disparos.click_button_add_new_rule()


@then(u'selecionar a regra "{rule}" no dropdown "Escolha Uma Regra"')
def select_rule(context, rule):
    limitacao_disparos.select_option_new_rule(rule)
    limitacao_disparos.set_quantity()


@then(u'selecionar a regra "{rule}" no dropdown "Escolha Uma Regra" sem preencher a quantidade')
def select_rule_without_quantity(context, rule):
    limitacao_disparos.select_option_new_rule(rule)


@then(u'clicar no botão "Confirmar" para adicionar uma nova regra')
def confirm_rule_selected(context):
    clicar_btn_confirmar(context)


@then(u'esteja na janela de "Editar Limitação de Disparos"')
def access_page(context):
    click_edit_button(context)


@then(u'não selecionar uma regra no dropdown "Escolha Uma Regra" e preencher a quantidade')
def set_quantity(context):
    limitacao_disparos.set_quantity()


@then(u'selecionar a opção "teste" do dropdown "Carteira" na janela "Editar Limitação de Disparos" e Salvar')
def step_impl(context):
    select_carteira(context)


@then(u'a nova regra "{rule}" deve ser adicionada com sucesso')
def assert_rule_added(context, rule):
    name_rule = 'encontrada'
    try:
        time.sleep(1)
        limitacao_disparos.click_button_edit()
        lista = limitacao_disparos.get_table_page_editar_limitacao_disparos()
        assert fc.procurar_valor_lista_regras(lista, 'Regra', rule)
        logger.logging.info(constants.VALIDAR_REGRA % name_rule)
        limitacao_disparos.driver.save_screenshot(fc.name_screenshot_default(constants.VALIDAR_REGRA % rule))
    except Exception as e:
        logger.logging.info(constants.ERRO_VALIDAR_REGRA % name_rule)
        logger.logging.info(e)
        raise


@then(u'a janela inicial para editar "Limitação Disparos" das regras deverá ser aberta')
def validate_page_editar_limitacao_disparos(context):
    text_expected = 'Editar Limitação de Disparos'
    try:
        text_returned = limitacao_disparos.get_text_initial_edit_page()
        limitacao_disparos.driver.save_screenshot(
            fc.name_screenshot_default(constants.TEXTO_ENCONTRADO_SUCESSO % text_returned))
        assert text_returned == text_expected
        logger.logging.info(constants.PAGINA_VALIDADA_SUCESSO % text_returned)
    except Exception as e:
        logger.logging.error(constants.ERRO_VALIDAR_PAGINA % text_expected)
        logger.logging.error(e)
        raise
    finally:
        limitacao_disparos.driver.quit()


@then(
    u'a mensagem "{msg_expected}" deverá ser exibida abaixo do campo quantidade na janela "Editar Limitação de Disparos"')
def validate_mesage_warning_quantity(context, msg_expected):
    try:
        msg_returned = limitacao_disparos.get_text_mesage_warning_quantity()
        assert msg_returned == msg_expected
        logger.logging.info(constants.TEXTO_ENCONTRADO_SUCESSO % msg_expected)
    except Exception as e:
        logger.logging.error(constants.ERRO_ENCONTRAR_TEXTO)
        logger.logging.error(e)
        raise
    finally:
        limitacao_disparos.driver.quit()


@then(
    u'a mensagem "{msg_expected}" deverá ser exibida abaixo do dropdown "Escolha Uma Regra" na janela "Editar Limitação de Disparos"')
def validate_message_warning_rule(context, msg_expected):
    try:
        msg_returned = limitacao_disparos.get_text_mesage_warning_rule()
        assert msg_returned == msg_expected
        logger.logging.info(constants.TEXTO_ENCONTRADO_SUCESSO % msg_expected)
    except Exception as e:
        logger.logging.error(constants.ERRO_ENCONTRAR_TEXTO)
        logger.logging.error(e)
        raise
    finally:
        limitacao_disparos.driver.quit()


# TODO Realizar Validação da função abaixo
@then(u'o nome da Limitação de Disparos deve ser alterada para "Testes"')
def validate_changed_name(context):
    try:
        text_expected = limitacao_disparos.text
        logger.logging.info(text_expected)
    except Exception as e:
        logger.logging.error(e)
        raise
    finally:
        limitacao_disparos.driver.quit()


@then(u'a mensagem "{msg_expected}" deverá ser exibida na janela de "Editar Limitação de Disparo"')
def validate_mesage_error(context, msg_expected):
    try:
        msg_returned = limitacao_disparos.get_text_mesage_warning_name()
        assert msg_expected == msg_returned
        logger.logging.info(constants.TEXTO_ENCONTRADO_SUCESSO % msg_returned)
        limitacao_disparos.driver.save_screenshot(
            fc.name_screenshot_default(constants.TEXTO_ENCONTRADO_SUCESSO % msg_returned))
    except Exception as e:
        logger.logging.error(e)
        raise
    finally:
        limitacao_disparos.driver.quit()


@then(u'a carteira "teste" deverá ser alterada na janela de "Limitação de Disparos"')
def check_option_selected(context):
    try:
        time.sleep(2)
        limitacao_disparos.driver.save_screenshot(
            fc.name_screenshot_default('Carteira alterada para "Teste"'))
    except Exception as e:
        logger.logging.error(e)
        raise
    finally:
        limitacao_disparos.driver.quit()


@then(u'nenhuma alteração deve ser feita, mantendo os valores antigos')
def validate_not_changes(context):
    try:
        time.sleep(2)
        limitacao_disparos.driver.save_screenshot(
            fc.name_screenshot_default('Nenhuma alteração foi feita'))
    except Exception as e:
        logger.logging.error(e)
        raise
    finally:
        limitacao_disparos.driver.quit()
