#language: pt

@all @gestor @regras @ddd @incluir_novo_ddd
Funcionalidade: Incluir nova regra de DDD

  Contexto:
    Dado que esteja logado no produto "gestor-regras" com o usuário "gestor-adm"

  Cenário: Validar modal "Nova regra por DDD"
    E clicar no botão "Configurar" para incluir um novo "DDD"
    Quando clicar no botão "Novo DDD" para incluir uma nova regra da opção "DDD"
    Então o modal "NOVA REGRA POR DDD" deverá ser aberta

  Esquema do Cenário: Criar uma nova regra de "DDD" com dia de <day>
    E o modal "NOVA REGRA POR DDD" estiver aberta
    E selecionar o estado "<state>" com o DDD "<ddd>"
    E incluir a hora inicial e final no campo "Periodo de disparo semanal"
    E incluir a hora inicial e final no campo "Adicionais" de "<day>"
    Quando clicar no botão "Incluir" no modal "NOVA REGRA POR DDD"
    Então a regra com o DDD "<ddd>" do estado "<state>" deverá ser criado com sucesso com o dia "<day>"
    Exemplos:
      | state    | ddd   | day     |
      | Amazonas | Todos | Semana  |
      | Amazonas | 97    | Sábado  |
      | Bahia    | 77    | Domingo |

  Cenário: Tentar incluir uma regra ja existente
    E tenha uma regra de DDD já criada
    Quando clicar no botão "Incluir" no modal "NOVA REGRA POR DDD"
    Então o modal "REGRA DUPLICADA" com a mensagem "Você está tentando criar uma regra que já existe! Por favor edite a regra existente." deverá ser exibida