#language: pt

@all @gestor @regras @ddd @editar_ddd
Funcionalidade: Regras Editar DDD

  Contexto:
    Dado que esteja logado no produto "gestor-regras" com o usuário "gestor-adm"

  Cenário: Validar modal editar regra "DDD"
    Quando clicar no botão "Configurar" para editar a regra de um "DDD"
    E clicar no botão para editar uma regra da opção "DDD"
    Então a janela para editar uma regra da opção "DDD" deverá ser aberta

  # Preferencialmente deixar esse cenário sempre ACIMA do cenário abaixo para evitar passos desnecessário
  Esquema do Cenário: Editar o campo "<field>" de uma regra existente com o dia da(e) <day>
    E o modal editar regra "DDD" estiver aberta
    Quando editar o campo "<field>" no modal "Editar regra por DDD" com o dia "<day>" e salvar
    Então a regra de "DDD" deverá ser editada corretamente com o dia "<day>"
    Exemplos:
      | field                      | day     |
      | Periodo de disparo semanal | Semana  |
      | Adicionais                 | Sábado  |
      | Adicionais                 | Domingo |

  # Preferencialmente deixar esse cenário sempre ABAIXO do cenário acima para evitar passos desnecessário
  Esquema do Cenário: Desmarcar o checkbox "<day>"
    E o modal editar regra "DDD" estiver aberta
    E o checkbox do dia "<day>" esteja marcado
    Quando desmarcar o checkbox do dia "<day>" do modal de "Editar regra por DDD" e salvar
    Então o checkbox de "<day>" do modal de "Editar regra por DDD" deverá ser desmarcado com sucesso
    Exemplos:
      | day     |
      | Sábado  |
      | Domingo |
