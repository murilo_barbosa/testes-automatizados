#language: pt

@all @gestor @regras @limitacao_disparo @editar_limitacao_disparos
Funcionalidade: Editar Limitação Disparos

  Contexto:
    Dado que esteja logado no produto "gestor-regras" com o usuário "gestor-adm"

  Cenário: Acessar a página inicial para Editar "Limitação Disparos"
    Quando clicar no botão "Editar" da opção "Limitação Disparos"
    Entao a janela inicial para editar "Limitação Disparos" das regras deverá ser aberta

  Esquema do Cenário: Incluir uma nova regra ao "Editar Limitação de Disparos"
    E esteja na janela de "Editar Limitação de Disparos"
    E clicar no botão "Adicionar Nova Regra" na janela "Editar Limitação de Disparos"
    E selecionar a regra "<rule>" no dropdown "Escolha Uma Regra"
    E clicar no botão "Confirmar" para adicionar uma nova regra
    Quando clicar no botão "Salvar" para adicionar uma nova regra
    Entao a nova regra "<rule>" deve ser adicionada com sucesso
    Exemplos:
      | rule               |
      | SMS total          |
      | E-mail total       |
      | Cartas total       |
      | Total              |
      | Cartas Massivo     |
      | Cartas Preventivo  |
      | Contatos de SMS    |
      | Contatos de E-mail |

  Cenário: Tentar Incluir uma nova regra sem preencher a quantidade ao "Editar Limitação de Disparos"
    E esteja na janela de "Editar Limitação de Disparos"
    E clicar no botão "Adicionar Nova Regra" na janela "Editar Limitação de Disparos"
    E selecionar a regra "SMS total" no dropdown "Escolha Uma Regra" sem preencher a quantidade
    Quando clicar no botão "Confirmar" para adicionar uma nova regra
    Entao a mensagem "Por favor selecione uma quantidade!" deverá ser exibida abaixo do campo quantidade na janela "Editar Limitação de Disparos"

  Cenário: Tentar Incluir uma nova regra sem preencher a regra ao "Editar Limitação de Disparos"
    E esteja na janela de "Editar Limitação de Disparos"
    E clicar no botão "Adicionar Nova Regra" na janela "Editar Limitação de Disparos"
    E não selecionar uma regra no dropdown "Escolha Uma Regra" e preencher a quantidade
    Quando clicar no botão "Confirmar" para adicionar uma nova regra
    Entao a mensagem "Por favor selecione um tipo de regra!" deverá ser exibida abaixo do dropdown "Escolha Uma Regra" na janela "Editar Limitação de Disparos"

  Cenário: Alterar o Nome na janela de edição da Limitação de Disparos para Testes
    E esteja na janela de "Editar Limitação de Disparos"
    Quando alterar o "Nome" da janela de "Editar Limitação de Disparos" e Salvar
    Entao o nome da Limitação de Disparos deve ser alterada para "Testes"

  Cenário: Tentar alterar o Nome da Limitação de Disparos com o campo vazio
    E esteja na janela de "Editar Limitação de Disparos"
    Quando deixar o campo "Nome" vazio e salvar
    Entao a mensagem "Insira um nome para a regra" deverá ser exibida na janela de "Editar Limitação de Disparo"

  Cenário: Alterar a Carteira na janela de edição da Limitação de Disparos
    E esteja na janela de "Editar Limitação de Disparos"
    Quando selecionar a opção "teste" do dropdown "Carteira" na janela "Editar Limitação de Disparos" e Salvar
    Entao a carteira "teste" deverá ser alterada na janela de "Limitação de Disparos"

  Cenário: Alterar a Carteira e o Nome na janela de edição da Limitação de Disparos e clicar no botão Cancelar
    E esteja na janela de "Editar Limitação de Disparos"
    E selecionar a opção "teste" do dropdown "Carteira" na janela "Editar Limitação de Disparos" e Salvar
    Quando alterar o "Nome" da janela de "Editar Limitação de Disparos" para "Testes" e Cancelar
    Entao nenhuma alteração deve ser feita, mantendo os valores antigos