#language: pt

@all @gestor @regras @lista_restrita_dominios @visualizar_lista_restrita_dominios
Funcionalidade: Visualizar Regra "Lista Restrita Domínios"

  Contexto:
    Dado que esteja logado no produto "gestor-regras" com o usuário "gestor-adm"

  Cenário: Acessar a página inicial "Lista Restrita Domínios"
    Quando clicar no botão "Configurar" da opção "Lista Restrita Domínios"
    Então a janela inicial "Lista Restrita Domínios" das regras deverá ser aberta

  Esquema do Cenário: Na página de "Regras", <status> a regra "Lista Restrita Domínios"
    Quando clicar no botão "<status>" da opção "Lista Restrita Domínios"
    Então a regra da opção "Lista Restrita Domínios" deverá estar "<result>"
    Exemplos:
      | status      | result       |
      | Desabilitar | Desabilitada |
      | Habilitar   | Habilitada   |

  Esquema do Cenário: Na página "Lista Restrita Domínios", alterar a ordenação de "<condition_1>" para "<condition_2>"
    E esteja na janela de "Lista Restrita Domínios"
    Quando alterar a ordenação de "<condition_1>" para "<condition_2>" na janela de "Lista Restrita Domínios"
    Então a ordenação por "<condition_2>" deverá ser exibida de forma "crescente" na janela de "Lista Restrita Domínios"
    Exemplos:
      | condition_1        | condition_2        |
      | Data               | Extensões Domínios |
      | Extensões Domínios | Data               |

  Cenário: Na página "Lista Restrita Domínios", reordenar a lista de forma "Decrescente"
    E esteja na janela de "Lista Restrita Domínios"
    Quando clicar no botão para reordenar a ordem de exibição na janela de "Lista Restrita Domínios"
    Então a ordenação deverá ser exibida de forma "decrescente" na janela de "Lista Restrita Domínios"


  @todo # Verificar um meio eficiente de validar o teste. (Talvez fazendo um document.query direto pelo JS)
#  Esquema do Cenário: Realizar buscas por "<condition>"
#    Quando clicar no botão "Configurar" da opção "DDD"
#    E realizar uma busca por um "<condition>"
#    Então todas as regras com o "<condition>" selecionado deverá ser exibida
#    Exemplos:
#      | condition |
#      | Estado    |
#      | DDD       |