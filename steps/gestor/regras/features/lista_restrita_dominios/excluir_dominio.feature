#language: pt

@all @gestor @regras @lista_restrita_dominios @excluir_dominio_restrito
Funcionalidade: Excluir uma nova restrição "Lista Restrita Domínios"

  Contexto:
    Dado que esteja logado no produto "gestor-regras" com o usuário "gestor-adm"

  Cenário: Excluir uma extesão na janela de "Lista Restrita Domínios"
    E esteja na página incial da opção "Lista Restrita Domínios"
    E selecionar um dominio para ser excluído
    Quando clicar no botão "Excluir" da janela inicial "Lista Restrita Domínios"
    Então o dominio selecionado não deverá ser listado na janela "Lista Restrita Domínios"