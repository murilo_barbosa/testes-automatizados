import time
from behave import given, when, then
from features.steps.gestor.regras.functions.Ddd import Ddd
from features.utils import logger, constants, file_comum as fc

ddd = Ddd()
dir_screenshot = 'gestor/regras/ddd/incluir_novo_ddd'
hora_inicio = '08'
hora_final = '18'


@given(u'clicar no botão "Configurar" para incluir um novo "DDD"')
def clicar_botao_configurar_ddd(context):
    global ddd
    ddd = Ddd()
    ddd.clicar_botao_configurar_ddd()


@when(u'clicar no botão "Novo DDD" para incluir uma nova regra da opção "DDD"')
def clicar_botao_novo_ddd(context):
    ddd.clicar_botao_novo_ddd()


@when(u'clicar no botão "Incluir" no modal "NOVA REGRA POR DDD"')
def clicar_botao_incluir(context):
    ddd.clicar_botao_incluir_regra()
    time.sleep(1)


@then(u'incluir a hora inicial e final no campo "Periodo de disparo semanal"')
def incluir_hora_nova_regra(context):
    global hora_inicio, hora_final
    time.sleep(1)
    ddd.incluir_hora(ddd.dia_semana, hora_inicio, hora_final)


@then(u'selecionar o estado "{state}" com o DDD "{cod_ddd}"')
def selecionar_estado_e_ddd(context, state, cod_ddd):
    try:
        ddd.selecionar_ddd(state, cod_ddd)
    except Exception as e:
        logger.logging.error(e)
        raise


@then(u'o modal "NOVA REGRA POR DDD" estiver aberta')
def abrir_modal_nova_regra_ddd(context):
    clicar_botao_configurar_ddd(context)
    clicar_botao_novo_ddd(context)


@then(u'tenha uma regra de DDD já criada')
def pegar_regra_criada(context):
    clicar_botao_configurar_ddd(context)
    ddd.clicar_botao_novo_ddd()
    incluir_hora_nova_regra(context)
    regra_criada = ddd.pegar_tabela_regras_ddd()[0]
    if regra_criada is []:
        ddd.selecionar_ddd('Acre', '68')
        ddd.clicar_botao_incluir_regra()
        pegar_regra_criada(context)
    else:
        ddd.selecionar_ddd(regra_criada[ddd.texto_estado], regra_criada[ddd.texto_ddd])


@then(u'incluir a hora inicial e final no campo "Adicionais" de "{day}"')
def incluir_hora_adicionais(context, day):
    global hora_inicio, hora_final
    if day == 'Semana':
        return
    try:
        status_checkbox = ddd.verificar_status_checkbox_modal_incluir_regra(day)
        if status_checkbox is False:
            ddd.selecionar_checkbox_modal_incluir(day)
        ddd.incluir_hora(day, hora_inicio, hora_final)
    except Exception as e:
        logger.logging.error(e)
        raise


@then(u'o modal "NOVA REGRA POR DDD" deverá ser aberta')
def validar_modal_nova_regra_ddd(context):
    texto_esperado = 'NOVA REGRA POR DDD'
    try:
        texto_retornado = ddd.pegar_texto_modal_incluir_ddd()
        assert texto_esperado == texto_retornado
        logger.logging.info(constants.PAGINA_VALIDADA_SUCESSO % texto_esperado)
        ddd.driver.save_screenshot(fc.name_screenshot_default(constants.PAGINA_VALIDADA_SUCESSO % texto_esperado))
    except Exception as e:
        logger.logging.error(constants.ERRO_VALIDAR_PAGINA % texto_esperado)
        logger.logging.error(constants.ERRO_GENERICO % e)
        raise
    finally:
        ddd.driver.quit()


@then(u'a regra com o DDD "{cod_ddd}" do estado "{state}" deverá ser criado com sucesso com o dia "{day}"')
def validar_nova_regra_criada(context, cod_ddd, state, day):
    global hora_inicio, hora_final
    texto_pagina = 'Nova regra por DDD'
    try:
        time.sleep(2)
        regra = ddd.pegar_tabela_regras_ddd()[0]
        estado_retornado = regra["Estado"]
        ddd_retornado = regra["DDD"]
        hora_inicio_retornado, hora_final_retornado = ddd.pegar_horario_primeira_linha(day)
        assert estado_retornado == state \
               and ddd_retornado == cod_ddd \
               and hora_inicio_retornado == hora_inicio \
               and hora_final_retornado == hora_final
        logger.logging.info(constants.PAGINA_VALIDADA_SUCESSO % texto_pagina)
        ddd.driver.save_screenshot(fc.name_screenshot_default(constants.PAGINA_VALIDADA_SUCESSO % texto_pagina))
    except Exception as e:
        logger.logging.error(constants.ERRO_VALIDAR_PAGINA % texto_pagina)
        logger.logging.error(e)
        raise
    finally:
        time.sleep(1)
        ddd.clicar_botao_excluir_regra(screenshot=False)
        ddd.clicar_botao_confirmar_remover_regra(screenshot=False)
        ddd.driver.quit()


@then(u'o modal "{rule_text}" com a mensagem "{text_expected}" deverá ser exibida')
def validar_modal_regra_duplicada(context, rule_text, text_expected):
    texto_retornado = ddd.pegar_texto_modal_regra_duplicada()
    try:
        assert text_expected == texto_retornado
        logger.logging.info(constants.PAGINA_VALIDADA_SUCESSO % rule_text)
    except Exception as e:
        logger.logging.error(constants.ERRO_VALIDAR_PAGINA % rule_text)
        logger.logging.error(e)
        raise
    finally:
        screenshot = 'tentar_incluir_regra_repetida'
        ddd.driver.save_screenshot(fc.name_screenshot_default(dir_screenshot, screenshot))
        ddd.driver.quit()
