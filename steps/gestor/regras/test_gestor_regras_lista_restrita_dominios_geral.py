import time
from behave import given, when, then
from features.steps.gestor.regras.functions.ListaRestritaDominios import ListaRestritaDominios
from features.steps.gestor.regras.functions.Regras import Regras
from features.utils import logger, constants

lista_restrita = ListaRestritaDominios()


@given(u'esteja na página incial da opção "Lista Restrita Domínios"')
def acesar_pagina_inicial(context):
    global lista_restrita
    lista_restrita = ListaRestritaDominios()
    lista_restrita.clicar_botao_configurar()


@when(u'clicar no botão "{condition}" da opção "Lista Restrita Domínios"')
def clicar_botao(context, condition):
    global lista_restrita
    lista_restrita = ListaRestritaDominios()
    try:
        if condition == lista_restrita.texto_botao_desabilitar:
            lista_restrita.clicar_botao_desabilitar()
            lista_restrita.clicar_botao_confirmar_desabilitar_regra()
        elif condition == lista_restrita.texto_botao_habilitar:
            lista_restrita.clicar_botao_habilitar()
            lista_restrita.clicar_botao_confirmar_habilitar_regra()
        elif condition == lista_restrita.texto_botao_configurar:
            lista_restrita.clicar_botao_configurar()
        else:
            logger.logging.error(constants.VALOR_NAO_ENCONTRADO % condition)
            raise
    except Exception as e:
        logger.logging.error(e)
        raise


@when(u'clicar no botão para reordenar a ordem de exibição na janela de "Lista Restrita Domínios"')
def reordenar_exibicao(context):
    try:
        lista_restrita.clicar_botao_reordenar()
    except Exception as e:
        logger.logging.error(e)
        raise


@when(u'alterar a ordenação de "{condition_1}" para "{condition_2}" na janela de "Lista Restrita Domínios"')
def alterar_ordenacao(context, condition_1, condition_2):
    try:
        regras = Regras()
        regras.clicar_dropdown_ordenar_por()
        if condition_2 == 'Data':
            regras.alterar_ordenacao(condition_2, condition_1)
            regras.clicar_dropdown_ordenar_por()
        regras.alterar_ordenacao(condition_1, condition_2)
        time.sleep(1)
    except Exception as e:
        logger.logging.error(e)
        raise


@then(u'esteja na janela de "Lista Restrita Domínios"')
def acessar_janela_lista_restrita(context):
    clicar_botao(context, lista_restrita.texto_botao_configurar)
    texto_retornanto = lista_restrita.pegar_texto_janela_inicial()
    try:
        assert texto_retornanto == lista_restrita.texto_janela_inicial
        logger.logging.info(constants.PAGINA_VALIDADA_SUCESSO % texto_retornanto)
    except Exception as e:
        logger.logging.error(constants.ERRO_VALIDAR_PAGINA % texto_retornanto)
        logger.logging.error(e)
        raise


@then(u'a regra da opção "Lista Restrita Domínios" deverá estar "{status}"')
def validar_regra_habilitada_desabilitada(context, status):
    status_retornado = lista_restrita.validar_status_regra(status)
    try:
        assert status_retornado == status
        logger.logging.info(constants.VALIDAR_REGRA % status_retornado)
    except Exception as e:
        logger.logging.error(constants.ERRO_VALIDAR_REGRA % status_retornado)
        logger.logging.error(e)
        raise
    finally:
        lista_restrita.driver.quit()


@then(u'a janela inicial "Lista Restrita Domínios" das regras deverá ser aberta')
def validar_janela_inicial(context):
    texto_retornanto = lista_restrita.pegar_texto_janela_inicial()
    try:
        assert texto_retornanto == lista_restrita.texto_janela_inicial
        logger.logging.info(constants.PAGINA_VALIDADA_SUCESSO % texto_retornanto)
    except Exception as e:
        logger.logging.error(constants.ERRO_VALIDAR_PAGINA % texto_retornanto)
        logger.logging.error(e)
        raise
    finally:
        lista_restrita.driver.quit()


@then(
    u'a ordenação por "{condition}" deverá ser exibida de forma "{type_order}" na janela de "Lista Restrita Domínios"')
def validar_ordenacao(context, condition, type_order):
    try:
        # TODO
        #  Precisa encontrar um método eficiente de fazer a validação do temporario_pdf. Pois a tabela do HTML não sofre
        #  alterações após a ordenação da tabela. Usar OCR ou query do js para pegar o resultado.
        lista_restrita.pegar_texto_janela_inicial()
        #     lista = lista_restrita.pegar_tabela_regras_lista_restrita_dominios()
        #     logger.logging.info(lista)
        #     valor_primeira_linha = lista[0][condition]
        #     logger.logging.info(valor_primeira_linha)
        #     valor_segunda_linha = lista[1][condition]
        #     logger.logging.info(valor_segunda_linha)
        #
        #     if type_order == 'crescente':
        #         assert valor_primeira_linha <= valor_segunda_linha
        #         logger.logging.info('VALIDOU')
        #     elif type_order == 'decrescente':
        #         assert valor_primeira_linha >= valor_segunda_linha
        #     else:
        #         logger.logging.error(constants.VALOR_NAO_ENCONTRADO % type_order)
        #         raise
        logger.logging.info(constants.ORDENACAO_VALIDADA_SUCESSO % condition)
    except Exception as e:
        logger.logging.error(constants.ERRO_VALIDAR_ORDENACAO % condition)
        logger.logging.error(e)
        raise
    finally:
        lista_restrita.driver.quit()


@then(u'a ordenação deverá ser exibida de forma "decrescente" na janela de "Lista Restrita Domínios"')
def validar_ordenacao_botao_reordenar(context):
    time.sleep(1)
    validar_ordenacao(context, 'Data', 'decrescente')
