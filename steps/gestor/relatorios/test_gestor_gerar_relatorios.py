from behave import when, then
from features.steps.gestor.relatorios.functions.Relatorios import GerarRelatorios
from features.utils import logger, constants, file_comum as fc

gerar_relatorios = GerarRelatorios()
diretorio_raiz_capturas = 'gestor/relatorios'


@when(u'selecionar o submenu para "{condition} relatórios" no menu "Relatórios"')
def selecionar_submenu_em_relatorios(context, condition):
    global gerar_relatorios
    gerar_relatorios = GerarRelatorios()
    gerar_relatorios.clicar_menu_relatorios()
    gerar_relatorios.clicar_submenu(condition)


@then(u'selecionar a opção "{opcao}" no dropdown "Tipo de Relatório"')
def selecionar_opcao_gerar_relatorios(context, opcao):
    selecionar_submenu_em_relatorios(context, 'Gerar')
    gerar_relatorios.clicar_dropdown_tipo_relatorio()
    gerar_relatorios.selecionar_opcao_tipo_relatorio(opcao)


@then(u'selecionar a opção "{opcao}" no dropdown "Gestor"')
def selecionar_gestor(context, opcao):
    gerar_relatorios.clicar_dropdown_gestor()
    gerar_relatorios.selecionar_opcao_gestor(opcao)


@then(u'selecionar a opção "{opcao}" no dropdown "Período"')
def selecionar_opcao_periodo(context, opcao):
    gerar_relatorios.clicar_dropdown_periodo()
    gerar_relatorios.selecionar_opcao_periodo(opcao)


@when(u'clicar no botão "Gerar" na janela de "Gerar relatórios"')
def clicar_botao_gerar(context):
    gerar_relatorios.clicar_botao_gerar()


@then(u'a janela de "{condition} relatórios" deverá ser aberta')
def validar_janela_relatorios(context, condition):
    texto_esperado = f'{condition} relatórios'
    texto_retornado = str(gerar_relatorios.verificar_janela_relatorios())
    try:
        assert texto_esperado == texto_retornado
        logger.logging.info(constants.PAGINA_VALIDADA_SUCESSO % texto_esperado)
    except Exception:
        logger.logging.error(constants.ERRO_COMPARACAO_MENSAGEM % (texto_esperado, texto_retornado))
        raise
    finally:
        gerar_relatorios.driver.quit()


@then(u'o relatório "{tipo_relatorio}" deverá ser gerado com a data "{periodo}"')
def validar_periodo_relatorio(context, tipo_relatorio, periodo):
    valor_retornado = None
    try:
        valor_retornado = gerar_relatorios.validar_periodo_relatorio_gerado(tipo_relatorio, periodo)
        assert valor_retornado
        logger.logging.info(constants.RELATORIO_VALIDADO_SUCESSO % periodo)
        gerar_relatorios.driver.save_screenshot(
            fc.name_screenshot_default(constants.RELATORIO_VALIDADO_SUCESSO % periodo))
    except Exception:
        logger.logging.error(constants.ERRO_COMPARACAO_MENSAGEM % (True, valor_retornado))
        raise
    finally:
        gerar_relatorios.driver.quit()
