from datetime import datetime
import time
from selenium.webdriver.common.by import By
from features.utils import file_comum as fc, logger, constants


class GerarRelatorios(object):
    """Classe responsável por gerar os Relatórios do Gestor."""

    def __init__(self):
        self.driver = fc.get_driver()

    def clicar_menu_relatorios(self):
        """Clicando no menu de Relatórios"""
        link_text = 'Relatórios'
        try:
            self.driver.save_screenshot(fc.name_screenshot_default(constants.CLICANDO_MENU % link_text))
            self.driver.find_element(By.LINK_TEXT, link_text).click()
            logger.logging.info(constants.CLICANDO_MENU % link_text)
            time.sleep(1)
        except Exception:
            logger.logging.error(constants.ERRO_CLICAR_MENU % link_text)
            raise

    def clicar_submenu(self, opcao):
        """Clicando no submenu do menu Relatórios"""
        try:
            self.driver.save_screenshot(fc.name_screenshot_default(constants.CLICANDO_MENU % opcao))
            self.driver.find_element(By.LINK_TEXT, opcao).click()
            logger.logging.info(constants.CLICANDO_MENU % opcao)
            time.sleep(1)
        except Exception:
            logger.logging.error(constants.ERRO_CLICAR_MENU % opcao)
            raise

    def verificar_janela_relatorios(self):
        """Validando se a página aberta é mesmo a de Gerar ou Download de relatórios."""
        time.sleep(1)
        try:
            texto = self.driver.find_element(By.XPATH, '/html/body/section/div/div[5]/div[2]/div/div[1]/div/h1').text
            self.driver.save_screenshot(fc.name_screenshot_default(constants.PAGINA_VALIDADA_SUCESSO % texto))
            return texto
        except Exception as e:
            logger.logging.error(constants.ERRO_VALIDAR_PAGINA % e)
            raise

    def clicar_dropdown_tipo_relatorio(self):
        """Clicar no menu Relatório"""
        dropdown = 'Tipo de Relatório'
        try:
            self.driver.find_element(By.ID, 'selectedReportType').click()
            logger.logging.info(constants.CLICANDO_DROPDOWN % dropdown)
            self.driver.save_screenshot(fc.name_screenshot_default(constants.CLICANDO_DROPDOWN % dropdown))
        except Exception:
            logger.logging.error(constants.ERRO_CLICAR_DROPDOWN % dropdown)
            raise

    def selecionar_opcao_tipo_relatorio(self, opcao):
        """Selecionar o tipo de Relatório"""
        texto_opcao = 'Tipo de Relatório'
        try:
            self.driver.find_element(By.ID, f'{opcao}report').click()
            logger.logging.info(constants.SELECIONADO_OPCAO_DROPDOWN % (opcao, texto_opcao))
            self.driver.save_screenshot(
                fc.name_screenshot_default(constants.SELECIONADO_OPCAO_DROPDOWN % (opcao, texto_opcao)))
        except Exception:
            logger.logging.error(constants.ERRO_SELECIONAR_OPCAO % texto_opcao)
            raise

    def clicar_dropdown_gestor(self):
        """Clicar no dropdown Gestor"""
        dropdown = 'Gestor'
        try:
            self.driver.find_element(By.XPATH, '//*[@id="multiselectCont"]/span/div/button').click()
            logger.logging.info(constants.CLICANDO_DROPDOWN % dropdown)
            self.driver.save_screenshot(fc.name_screenshot_default(constants.CLICANDO_DROPDOWN % dropdown))
        except Exception:
            logger.logging.error(constants.ERRO_CLICAR_DROPDOWN % dropdown)
            raise

    def selecionar_opcao_gestor(self, opcao):
        """Selecionar a opção do dropdown Gestor"""
        texto_opcao = 'Gestor'
        try:
            self.driver.find_element(By.XPATH,
                                     '//*[@id="multiselectCont"]/span/div/ul/li[3]/a/label/input').click()
            logger.logging.info(constants.SELECIONADO_OPCAO_DROPDOWN % (opcao, texto_opcao))
            self.driver.find_element(By.XPATH, '//*[@id="multiselectCont"]/span/div/button').click()
        except Exception:
            logger.logging.error(constants.ERRO_SELECIONAR_OPCAO % texto_opcao)
            raise

    def clicar_dropdown_periodo(self):
        """Clicar no dropdown Período"""
        dropdown = 'Período'
        try:
            self.driver.find_element(By.ID, "reportrange").click()
            logger.logging.info(constants.CLICANDO_DROPDOWN % dropdown)
            self.driver.save_screenshot(fc.name_screenshot_default(constants.CLICANDO_DROPDOWN % dropdown))
        except Exception:
            logger.logging.error(constants.ERRO_CLICAR_DROPDOWN % dropdown)
            raise

    def selecionar_opcao_periodo(self, data):
        """Selecionar opção do dropdown Período"""
        element = None
        try:
            match data:
                case 'Hoje':
                    element = 1
                case 'Ontem':
                    element = 2
                case 'Últimos 7 dias':
                    element = 3
                case 'Últimos 30 dias':
                    element = 4
                case 'Este mês':
                    element = 5
                case 'Mês passado':
                    element = 6
                case 'Outro período':
                    element = 7
            self.driver.find_element(By.XPATH, f'/html/body/div[3]/div[3]/ul/li[{str(element)}]').click()
            logger.logging.info(constants.SELECIONADO_OPCAO_DROPDOWN % (data, 'Período'))
            self.driver.save_screenshot(
                fc.name_screenshot_default(constants.SELECIONADO_OPCAO_DROPDOWN % (data, 'Período')))
        except Exception:
            logger.logging.error(constants.ERRO_SELECIONAR_OPCAO % data)
            raise

    def clicar_botao_gerar(self):
        """Clicar no botão Gerar"""
        texto_botao = 'Gerar'
        try:
            fc.move_scroll_bar(self.driver)
            self.driver.save_screenshot(fc.name_screenshot_default(constants.CLICANDO_BOTAO % texto_botao))
            self.driver.find_element(By.XPATH, '/html/body/section/div/div[5]/div[2]/div/div[3]/button').click()
            logger.logging.info(constants.CLICANDO_BOTAO % texto_botao)
            time.sleep(3)
        except Exception:
            logger.logging.error(constants.ERRO_CLICAR_BOTAO % texto_botao)
            raise

    def pegar_data_tipo_relatorio(self, tipo_relatorio):
        """Pega a data inicial e final conforme o tipo de relatório"""
        try:
            match tipo_relatorio:
                case 'Analítico':
                    tab = '1'
                case 'Sintético':
                    tab = '2'
                case 'API Analítico':
                    tab = '3'
                case 'Inválidos':
                    tab = '4'
                case 'Respostas':
                    tab = '5'
                case _:
                    logger.logging.error(constants.VALOR_NAO_ENCONTRADO % tipo_relatorio)
                    raise
            data_inicio = self.driver.find_element_by_xpath(f'//*[@id="tab1_{tab}"]/div/div[3]/div[3]/p[2]').text[-10:]
            data_final = self.driver.find_element_by_xpath(f'//*[@id="tab1_{tab}"]/div/div[3]/div[3]/p[3]').text[-10:]
            return data_inicio, data_final
        except Exception as e:
            logger.logging.error(constants.ERRO_GENERICO % e)
            raise

    def validar_periodo_relatorio_gerado(self, tipo_relatorio, periodo):
        """Validar se o relatório foi gerado corretamente conforme o período e o tipo de relatório informado"""
        formato_data = '%d/%m/%Y'
        try:
            data_inicio, data_final = self.pegar_data_tipo_relatorio(tipo_relatorio)
            data_atual = datetime.date(datetime.now()).strftime(formato_data)
            mes_atual = data_atual.split('/')[1]
            mes_inicio = data_inicio.split('/')[1]
            dia_inicio = data_inicio.split('/')[0]
            dia_final = data_final.split('/')[0]
            match periodo:
                case 'Hoje':
                    return True if data_atual == data_final else False
                case 'Ontem':
                    diferenca_dias = fc.calcular_diferenca_data_dias(data_inicio, data_atual)
                    return True if data_inicio == data_final and diferenca_dias == 1 else False
                case 'Últimos 7 dias':
                    diferenca_dias = fc.calcular_diferenca_data_dias(data_inicio, data_atual)
                    return True if diferenca_dias == 6 else False
                case 'Últimos 30 dias':
                    diferenca_dias = fc.calcular_diferenca_data_dias(data_inicio, data_atual)
                    return True if diferenca_dias == 29 else False
                case 'Este mês':
                    return True if mes_atual == mes_inicio and dia_inicio == '01' else False
                case 'Mês passado':
                    return True if mes_atual != mes_inicio and dia_inicio == '01' and dia_final >= '28' else False
                case 'Outro período':
                    pass
                case _:
                    logger.logging.error(constants.VALOR_NAO_ENCONTRADO % periodo)
                    raise
        except Exception:
            logger.logging.error(constants.ERRO_VALIDAR_PAGINA % periodo)
            raise
