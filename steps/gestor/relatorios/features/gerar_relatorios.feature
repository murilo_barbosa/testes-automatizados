#language: pt

@all @gestor @relatorios @gerar_relatorio
Funcionalidade: Gerar relatórios

  Contexto:
    Dado que esteja logado no produto "gestor" com o usuário "gestor-scob"

  Esquema do Cenário: Acessar a página de "<condition> relatórios"
    Quando selecionar o submenu para "<condition> relatórios" no menu "Relatórios"
    Então a janela de "<condition> relatórios" deverá ser aberta
    Exemplos:
      | condition |
      | Gerar     |
      | Download  |

  Esquema do Cenário: Selecionar o tipo de relatório "<opcao>" e com periodo "<periodo>" selecionado
    E selecionar a opção "<opcao>" no dropdown "Tipo de Relatório"
    E selecionar a opção "Gestor-adm" no dropdown "Gestor"
    E selecionar a opção "<periodo>" no dropdown "Período"
    Quando clicar no botão "Gerar" na janela de "Gerar relatórios"
    Então o relatório "<opcao>" deverá ser gerado com a data "<periodo>"
    Exemplos:
      | opcao         | periodo         |
      | API Analítico | Hoje            |
      | Sintético     | Hoje            |
      | Analítico     | Hoje            |
      | Inválidos     | Hoje            |
      | Respostas     | Hoje            |
      | API Analítico | Ontem           |
      | Sintético     | Ontem           |
      | Analítico     | Ontem           |
      | Inválidos     | Ontem           |
      | Respostas     | Ontem           |
      | API Analítico | Últimos 7 dias  |
      | Sintético     | Últimos 7 dias  |
      | Analítico     | Últimos 7 dias  |
      | Inválidos     | Últimos 7 dias  |
      | Respostas     | Últimos 7 dias  |
      | API Analítico | Últimos 30 dias |
      | Sintético     | Últimos 30 dias |
      | Analítico     | Últimos 30 dias |
      | Inválidos     | Últimos 30 dias |
      | Respostas     | Últimos 30 dias |
      | API Analítico | Este mês        |
      | Sintético     | Este mês        |
      | Analítico     | Este mês        |
      | Inválidos     | Este mês        |
      | Respostas     | Este mês        |
      | API Analítico | Mês passado     |
      | Sintético     | Mês passado     |
      | Analítico     | Mês passado     |
      | Inválidos     | Mês passado     |
      | Respostas     | Mês passado     |

  @todo #Falta implementar
#      | API Analítico | Outro período   |
#      | Sintético     | Outro período   |
#      | Analítico     | Outro período   |
#      | Inválidos     | Outro período   |
#      | Respostas     | Outro período   |


