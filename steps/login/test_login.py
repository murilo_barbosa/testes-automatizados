from behave import given, when, then
from features.steps.login.function.Login import Login
from features.utils import global_variables as gv, file_comum

login = Login(None)
diretorio_raiz_capturas = 'login'


@given(u'que o usuário tente logar com o usuário "{user}"')
def set_user(context, user):
    global login
    login = Login(user=user, url=gv.url)
    login.set_user()
    login.set_usermame()


@given(u'o usuário digite a senha "{password}"')
def set_password(context, password):
    global login
    match password:
        case 'padrão':
            login.set_password()
        case 'incorreta':
            login.password = password
            login.set_password()


@when(u'pressionar o botão "Login"')
def press_button_login(context):
    global login
    login.press_button_login()


@then(u'o usuário deve ser logado com "{message}"')
def check_if_user_is_logged(context, message):
    try:
        if message == 'sucesso':
            login.check_user_logged()
        elif message == 'falha':
            login.check_user_not_logged()
    except Exception:
        raise
    finally:
        caminho_captura = file_comum.name_screenshot_default(diretorio_raiz_capturas, f'login_{message}')
        login.driver.save_screenshot(caminho_captura)
        login.driver.quit()
