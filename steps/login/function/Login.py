import time
from selenium.webdriver.common.by import By
from features.support.Credentials import Credentials
from features.utils import logger, constants
from features.utils import file_comum as fc


class Login(object):
    """Classe responsavel por fazer o login no sistema"""

    def __init__(self, user=None, url=None, password=Credentials.password_default):
        self.url = url
        self.user = user
        self.password = password
        self.credentials = Credentials()
        self.driver = fc.get_driver()

    def set_user(self):
        """Selecionar o usuário a ser logado"""
        try:
            match self.user:
                case 'gestor':
                    self.user = Credentials.user_gestor
                case 'gestor-scob':
                    self.user = Credentials.user_gestor_scob
                case 'pontal':
                    self.user = Credentials.user_pontal
                case _:
                    self.user = self.user
            return self.user
        except Exception as e:
            logger.logging.error(constants.ERRO_GENERICO % e)
            raise

    def set_usermame(self):
        """Digitando o nome do usuário para logar"""
        el_name_username = '_username'
        try:
            self.driver.find_element(By.NAME, el_name_username).send_keys(self.user)
            logger.logging.info(constants.DIGITANDO_USUARIO % self.user)
        except Exception as e:
            logger.logging.error(constants.ERRO_GENERICO % e)
            raise

    def set_password(self):
        """Digitando a senha do usuário para logar"""
        el_name_password = '_password'
        try:
            self.driver.find_element(By.NAME, el_name_password).send_keys(self.password)
            logger.logging.info(constants.DIGITANDO_SENHA % 'padrão')
        except Exception as e:
            logger.logging.error(constants.ERRO_GENERICO % e)
            raise

    def press_button_login(self):
        """Pressionando o botão LOGIN para efetuar o login"""
        el_xpath_button_login = '//button[normalize-space()="Login"]'
        try:
            self.driver.save_screenshot(fc.name_screenshot_default('Fazendo Login'))
            self.driver.find_element(By.XPATH, el_xpath_button_login).click()
            logger.logging.info(constants.CLICANDO_BOTAO % 'Login')
        except Exception as e:
            logger.logging.error(constants.ERRO_GENERICO % e)
            raise

    def check_user_logged(self):
        """Validando se o usuário logado é o mesmo que foi inserido no login"""
        time.sleep(2)
        try:
            element = self.driver.find_element(By.CLASS_NAME, 'logopanel')
            texto_sucesso = element.text
            if texto_sucesso == self.credentials.get_user_name(self.user):
                logger.logging.info(constants.LOGIN_SUCESSO)
        except Exception as e:
            logger.logging.error(constants.LOGIN_FALHA % self.user)
            logger.logging.error(constants.ERRO_GENERICO % e)
            raise

    def check_user_not_logged(self):
        try:
            element = self.driver.find_element(By.CLASS_NAME, 'alert.alert-danger')
            texto_falha = element.text
            if texto_falha == 'Usuário ou senha inválidos':
                logger.logging.info(texto_falha)
        except Exception as e:
            logger.logging.error(constants.LOGIN_SUCESSO)
            logger.logging.error(constants.ERRO_GENERICO % e)
            raise
