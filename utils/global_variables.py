import os

"""
- Conjunto de variáveis úteis compartilhadas no projeto.
- Usar o padrão para aliás (apelido para o import) de gv.
"""

# URLs
url_gestor = None
url_gestor_base = None
url = None
url_gestor_regras = None

# VARIABLES
driver = None
browser = 'chrome'
headless = False
dir_pdf = 'temporario_pdf'
path_full_dir_pdf = f'{os.path.abspath(os.getcwd())}/features/support/arquivos/screenshots/temporario_pdf'
